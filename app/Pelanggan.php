<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pelanggan extends Model
{
    protected $fillable = [
        'nometer', 'nama', 'alamat', 'kode_tarif'
    ];
}
